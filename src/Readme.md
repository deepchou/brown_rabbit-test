Framework and functionality
===========================
* I used Bootstrap 4 framework for responsiveness.
* For search Functionality - If you write anything in input box, it will search from blog list.
* For slider I used normal image change functionality with auto changes and next - previous buttons.


Points 
==========================
* From Bootstrap I used Navbar component, Flex utility and grid layout behavior. (I can also do this without framework if you need).
* I used Flex-box css properties for "Sponsors" img section just for example. (I could use Flex utility classes but I thought to show how we can do that with CSS properties).
* I tried to Keep the site's look same till width = 768px. And after that, changed most of the sections to 100%.
* You might see small small change in element's width and styling during screen size change. I write additional CSS for some of them for making them relevant to size.
* For Menu link active and hover, I used underline for making it more interactive.
* Also used hover effect for the "Read More" button and pagination links.
* In sidebar the spacing for the 1st element link is a bit different from PSD. I think for uniformity we need to maintain uniform space before links.
* For phone number and email in footer, I used href="tel" and href="mailto". After hovering over them the text will hilight and change it's color.

* I tried to add classes for every element to maintain specifity rule.
* I did not copied any bootstrap css but I used their links to featch the css and js code. But we can also copy them.