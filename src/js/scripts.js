/*jslint browser: true*/
/*global $, document*/
$(document).ready(function() {
    
    //search functionality code
    $('#search').keyup(function() {
        // Search input text value store in variable
        var text = $(this).val();
        
        // Hide all elements, where we don't want to search
        $('.blog-common-box, .hero-banner, .sponsors').hide();
        
        // Show search result blog (having input value text in it)
        $('.blog-common-box:contains("'+text+'")').show();
        
        // if search input is blank, show everything we hide initially
        if(text == '') {
            $('.blog-common-box, .hero-banner, .sponsors').show();
        }
        
        // for case sensitive search input
        $.expr[":"].contains = $.expr.createPseudo(function(arg) {
            return function( elem ) {
                return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
            };
        });
    });
    
    
    //Slider functionality code
    $(function() {
      var bannerOuter = $('.banner-outer');

      function slideNext() {
        bannerOuter.animate(300, function() {
          $('.banner-item').last().after($('.banner-item').first());
        });
      }
        
       //Enabling auto scroll
       sliderInterval = setInterval(slideNext, 3000);
        
       //scrolling on previous button
       $('.prev').on('click', function() {
        bannerOuter.animate(300, function() {
          $('.banner-item').first().before($('.banner-item').last());
        });

      });
        
      //scrolling on next button
      $('.next').on('click', function() {
        bannerOuter.animate(300, function() {
          $('.banner-item').last().after($('.banner-item').first());
        });
      });

    });
    
});